import sys
from collections import defaultdict

inputPath = "../input/input06.txt"
inputArray = []


def readInput():
    try:
        with open(inputPath, "r") as f:
            for line in f.readlines():
                inputArray.append(line.strip("\n"))

    except:
        print("Error while reading " + inputPath, file=sys.stderr)
        sys.exit(1)
    return


def getConnexions():
    connexions = defaultdict(list)

    for line in inputArray:
        planet, satellit = line.split(")")
        connexions[planet].append(satellit)

    return connexions


def countOrbits(planet, count, sum, connexions):
    sum += count
    for satellit in connexions[planet]:
        sum = countOrbits(satellit, count+1, sum, connexions)

    return sum

# Part TWO


def listOfStars():
    stars = []

    for line in inputArray:
        planet, satellit = line.split(")")
        stars.append(planet)
        stars.append(satellit)

    uniqueList = list(set(stars))

    return uniqueList


def getAdjacencyMatrix(listOfStars):
    nbStars = len(listOfStars)
    notLinked = 0
    linked = 1

    matrix = [[notLinked for x in range(nbStars)] for y in range(nbStars)]

    for line in inputArray:
        planet, satellit = line.split(")")
        indexPlanet = listOfStars.index(planet)
        indexSatellit = listOfStars.index(satellit)
        matrix[indexPlanet][indexSatellit] = linked
        matrix[indexSatellit][indexPlanet] = linked

    return matrix


def getStar(starIndex, listOfStars):
    return listOfStars[starIndex]


def adjacentyMatrixToGraph(adjacencyMatrix, listOfStars):
    return {getStar(i, listOfStars): [getStar(j, listOfStars) for j, adjacent in enumerate(row) if adjacent] for i, row in enumerate(adjacencyMatrix)}


def printAdjacentyMatrix(adjacencyMatrix, listOfStars):
    print(" ", end=" ")
    for i in range(0, len(listOfStars), 1):
        print(listOfStars[i], end=" ")

    print()

    for i in range(0, len(adjacencyMatrix), 1):
        print(listOfStars[i], end=" ")
        for j in range(0, len(adjacencyMatrix), 1):
            print(repr(adjacencyMatrix[i][j]), end=" ")

        print()


def findShortestPath(graph, start, end, path=[]):
    path = path + [start]
    if start == end:
        return path

    if (start not in graph):
        return None

    shortest = None

    for node in graph[start]:
        if (node not in path):
            newpath = findShortestPath(graph, node, end, path)

            if newpath:
                if not shortest or len(newpath) < len(shortest):
                    shortest = newpath
    return shortest


def find(node, graph):
    vertex = None
    for k, v in graph.items():
        if (k == node):
            vertex = v[0]
            break

    if (vertex == None):
        print("Error : " + node + " not found")
        sys.exit(1)

    return vertex


readInput()


def partOne():
    connexions = getConnexions()
    total = countOrbits("COM", 0, 0, connexions)
    print("Part one")
    print("Total : " + repr(total))
    return


def partTwo():
    l = listOfStars()
    adjacencyMatrix = getAdjacencyMatrix(l)
    graph = adjacentyMatrixToGraph(adjacencyMatrix, l)

    vertexYOU = find("YOU", graph)
    vertexSAN = find("SAN", graph)
    print("Shortest path between " + vertexYOU + " and " + vertexSAN)
    shortestPath = findShortestPath(graph, vertexYOU, vertexSAN)
    answer = len(shortestPath) - 1

    print("Part Two : " + repr(answer))
    return


partOne()
partTwo()
