import sys
inputPath = "../input/input04.txt"


def readInput():
    inputArray = None

    try:
        with open(inputPath, "r") as f:
            firstLine = f.readline()

        inputArray = [x for x in firstLine.split('-')]

    except:
        print("Error while reading " + inputPath, file=sys.stderr)
        sys.exit(1)

    return inputArray


inputArray = readInput()
minValue = int(inputArray[0])
maxValue = int(inputArray[1])

"""
    It is a six-digit number.
    The value is within the range given in your puzzle input.
    Two adjacent digits are the same (like 22 in 122345).
    Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).

"""


def hasAdjacentDigits(password):
    b = False
    for i in range(0, len(password)-1, 1):
        if (password[i] == password[i+1]):
            b = True
            break

    return b


def neverDecreases(password):
    b = True
    for i in range(1, len(password), 1):
        if (int(password[i-1]) > int(password[i])):
            b = False
            break

    return b


def isAValidPassword(password):
    b = True
    passwordInt = int(password)

    if (len(password) != 6):
        b = False

    elif (passwordInt < minValue) or (passwordInt > maxValue):
        b = False

    elif not(hasAdjacentDigits(password)):
        b = False

    elif (not(neverDecreases(password))):
        b = False

    return b


def nbValidPasswords():
    n = 0
    for i in range(minValue, maxValue+1, 1):
        password = repr(i)
        if (isAValidPassword(password)):
            n += 1

    return n


"""
PART TWO
An Elf just remembered one more important detail: the two adjacent matching digits are not part of a larger group of matching digits.

Given this additional criterion, but still ignoring the range rule, the following are now true:

    112233 meets these criteria because the digits never decrease and all repeated digits are exactly two digits long.
    123444 no longer meets the criteria (the repeated 44 is part of a larger group of 444).
    111122 meets the criteria (even though 1 is repeated more than twice, it still contains a double 22).
"""


def nbRepetitions(char, password):
    r = 0
    for i in range(0, len(password), 1):
        if (password[i] == char):
            r += 1

    return r


def getRepetitions(password):
    repetitions = []

    for i in range(0, len(password), 1):
        char = password[i]
        n = nbRepetitions(char, password)
        repetitions.append(n)

    return repetitions


"""
    If there is a number repeated more than twice, there has to be a number repeated exactly twice.
    Otherwise, there has to be a number repeated exactly twice.
"""


def checkAdjacents(password):
    repetitions = getRepetitions(password)
    hasADouble = (2 in repetitions)
    hasANumberRepeatedMoreThanTwice = False
    b = True

    for i in range(0, len(repetitions), 1):
        if (repetitions[i] > 2):
            hasANumberRepeatedMoreThanTwice = True
            break

    if (hasANumberRepeatedMoreThanTwice):
        b = hasADouble and hasANumberRepeatedMoreThanTwice

    else:
        b = hasADouble

    return b


def isAValidPasswordV2(password):
    b = True
    passwordInt = int(password)

    if (len(password) != 6):
        b = False

    elif (passwordInt < minValue) or (passwordInt > maxValue):
        b = False

    elif not(checkAdjacents(password)):
        b = False

    elif (not(neverDecreases(password))):
        b = False

    return b


def nbValidPasswordsV2():
    n = 0
    for i in range(minValue, maxValue+1, 1):
        password = repr(i)
        if (isAValidPasswordV2(password)):
            n += 1

    return n


def partOne():
    n = nbValidPasswords()
    print("Part one - Nb of valid passwords : " + repr(n))
    return


def partTwo():
    n = nbValidPasswordsV2()
    print("Part two - Nb of valid passwords : " + repr(n))
    return


partOne()
partTwo()
