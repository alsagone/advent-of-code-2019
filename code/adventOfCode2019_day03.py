import sys
import math
inputPath = "../input/input03.txt"


def readInput():
    paths = []
    try:
        with open(inputPath, "r") as f:
            lines = f.readlines()
            firstPath = [x.rstrip('\n') for x in lines[0].split(",")]
            secondPath = [x.rstrip('\n') for x in lines[1].split(",")]
            paths.append(firstPath)
            paths.append(secondPath)

    except:
        print("Error while reading " + inputPath, file=sys.stderr)
        sys.exit(1)

    return paths


def getDirection(str):
    return str[0]


def getNbSteps(str):
    return int(str[1:])


def move(x, y, direction, nbSteps):
    if (direction == "U"):
        index = 0
        increment = -1

    elif (direction == "D"):
        index = 0
        increment = 1

    elif (direction == "L"):
        index = 1
        increment = -1

    elif (direction == "R"):
        index = 1
        increment = 1

    else:
        print("Error : unknown direction (" + direction + ")")
        sys.exit(1)

    if (index == 0):
        x += increment * nbSteps

    else:
        y += increment * nbSteps

    return (x, y)


def getRoute(path):
    savedPath = []
    x = 1
    y = 1
    savedPath.append((1, 1))

    for i in range(0, len(path), 1):
        movement = path[i]
        direction = getDirection(movement)
        nbSteps = getNbSteps(movement)

        for i in range(1, nbSteps + 1, 1):
            coordinates = move(x, y, direction, 1)
            savedPath.append(coordinates)
            x = coordinates[0]
            y = coordinates[1]

    return savedPath


def checkDuplicates(coordinates, listOfCoordinates):
    for i in range(0, len(listOfCoordinates), 1):
        x = listOfCoordinates[i][0]
        y = listOfCoordinates[i][1]

        if (coordinates[0] == x) and (coordinates[1] == y):
            return True

    return False


def intersection(routeA, routeB):
    if (len(routeA) <= len(routeB)):
        shortestRoute = routeA
        longestRoute = routeB

    else:
        shortestRoute = routeB
        longestRoute = routeA

    _auxset = set(longestRoute)

    intersectionList = [x for x in shortestRoute if x in _auxset]

    intersectionList.remove((1, 1))

    return intersectionList


def manhattanDistance(xA, yA, xB, yB):
    return abs(xB - xA) + abs(yB - yA)


def shortestDistanceFromOrigin(intersectionList):
    xA = 1
    yA = 1
    minDistance = sys.maxsize

    for i in range(0, len(intersectionList), 1):
        xB = intersectionList[i][0]
        yB = intersectionList[i][1]

        minDistance = min(minDistance, manhattanDistance(xA, yA, xB, yB))

    return minDistance

# ............. PART TWO  .............


def nbStepsToReach(position, path):
    found = False

    for i in range(0, len(path), 1):
        if (path[i] == position):
            found = True
            break

    if (not(found)):
        print("Error : position not found", file=sys.stderr)
        sys.exit(1)

    return i


def shortestIntersectionToReach(intersectionList, firstRoute, secondRoute):
    nbSteps = sys.maxsize
    shortestIntersection = None

    for i in range(0, len(intersectionList), 1):
        position = intersectionList[i]
        stepsInFirstRoute = nbStepsToReach(position, firstRoute)
        stepsInSecondRoute = nbStepsToReach(position, secondRoute)
        sumSteps = stepsInFirstRoute + stepsInSecondRoute

        if (sumSteps < nbSteps):
            shortestIntersection = position
            nbSteps = sumSteps

    print("Shortest Intersection = " + repr(shortestIntersection))
    print("Nb steps = " + repr(nbSteps))
    return


paths = readInput()
firstPath = paths[0]
secondPath = paths[1]
firstRoute = getRoute(firstPath)
secondRoute = getRoute(secondPath)
intersectionList = intersection(firstRoute, secondRoute)


def partOne():
    print("Part one - Min distance = " +
          repr(shortestDistanceFromOrigin(intersectionList)))
    return


def partTwo():
    print("Part two")
    shortestIntersectionToReach(intersectionList, firstRoute, secondRoute)
    return


partOne()
partTwo()
