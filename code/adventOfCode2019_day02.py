import sys
inputPath = "../input/input02.txt"


def readInput():
    inputArray = None

    try:
        with open(inputPath, "r") as f:
            firstLine = f.readline()

        inputArray = [int(x) for x in firstLine.split(',')]

    except:
        print("Error while reading " + inputPath, file=sys.stderr)
        sys.exit(1)

    return inputArray


def partOne(inputArray):
    end = False
    i = 0

    while not(end):
        opcode = inputArray[i]

        if (opcode == 99):
            end = True

        else:
            position1 = inputArray[i+1]
            position2 = inputArray[i+2]
            position3 = inputArray[i+3]

            if (opcode == 1):
                inputArray[position3] = inputArray[position1] + \
                    inputArray[position2]

            elif (opcode == 2):
                inputArray[position3] = inputArray[position1] * \
                    inputArray[position2]

            else:
                print("Error : unknown opcode at inputArray [" + repr(i) + "] (" +
                      repr(opcode) + ")", file=sys.stderr)
                sys.exit(1)

            i += 4

    return inputArray[0]


def partTwoUtil(noun, verb):
    if ((noun > 99) or (verb < 0)):
        print("Error : answer not found")
        return False

    inputArray = readInput()
    inputArray[1] = noun
    inputArray[2] = verb
    result = partOne(inputArray)
    print("partTwoUtil(" + repr(noun) + "," +
          repr(verb) + ") = " + repr(result))
    if (result == 19690720):
        print("Noun = " + repr(noun))
        print("Verb = " + repr(verb))

        answer = 100 * noun + verb
        print("Answer = 100*" + repr(noun) + " + " +
              repr(verb) + " = " + repr(answer))
        return True

    else:
        return partTwoUtil(noun+1, verb-1)


def partTwo():
    inputArray = readInput()
    print("Part two")
    for noun in range(0, 100, 1):
        for verb in range(0, 100, 1):
            temp = inputArray.copy()
            temp[1] = noun
            temp[2] = verb

            result = partOne(temp)

            if (result == 19690720):
                print("\tNoun = " + repr(noun))
                print("\tVerb = " + repr(verb))
                answer = 100 * noun + verb
                print("\tPart two - Answer = 100*" + repr(noun) + " + " +
                      repr(verb) + " = " + repr(answer))
                return

    print("Error : answer not found", file=sys.stderr)
    return


inputArray = readInput()
inputArray[1] = 12
inputArray[2] = 2
answerPartOne = partOne(inputArray)
print("Part one : inputArray[0] = " + repr(answerPartOne))

partTwo()
