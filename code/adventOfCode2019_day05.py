import sys
inputPath = "../input/input05.txt"


def readInput():
    inputArray = None

    try:
        with open(inputPath, "r") as f:
            firstLine = f.readline()

        inputArray = [int(x) for x in firstLine.split(',')]

    except:
        print("Error while reading " + inputPath, file=sys.stderr)
        sys.exit(1)

    return inputArray

def parameterValues(inputArray, instruction, index):
    instructionArray = list(str(instruction))
    modes = ([0]*(5-len(instructionArray))) + instructionArray

    param1 = inputArray[index + 1] if (int(modes[2]) == 0) else (index+1)
    param2 = inputArray[index + 2] if (int(modes[1]) == 0) else (index+2)
    param3 = inputArray[index + 3] if (int(modes[0]) == 0) else (index+3)

    return param1, param2, param3


def process(myInput):
    inputArray = readInput()
    end = False
    i = 0

    while not(end):
        instruction = inputArray[i]

        opcode = instruction % 100
        param1, param2, param3 = parameterValues(inputArray, instruction, i)

        if (opcode == 1):
            inputArray[param3] = inputArray[param1] + \
                inputArray[param2]
            i += 4

        elif (opcode == 2):
            inputArray[param3] = inputArray[param1] * \
                inputArray[param2]
            i += 4

        elif (opcode == 3):
            inputArray[param1] = myInput
            i += 2

        elif (opcode == 4) or (opcode == 99):
            if (inputArray[param1] != 0) and (inputArray[i+2] == 99):
                print("Diagnostic tests ok - result : " +
                      repr(inputArray[param1]))
                end = True

            elif (inputArray[param1] != 0) and (inputArray[i+2] != 99):
                print(
                    "Diagnostic tests failed - result : " + repr(inputArray[param1]))
                end = True

            else:
                print("Diagnostic tests ok - result : " +
                      repr(inputArray[param1]))

            i += 2

        elif (opcode == 5):
            i = inputArray[param2] if (inputArray[param1] != 0) else (i + 3)

        elif (opcode == 6):
            i = inputArray[param2] if (inputArray[param1] == 0) else (i + 3)

        elif (opcode == 7):
            inputArray[param3] = 1 if (inputArray[param1] < inputArray[param2]) else 0
            i += 4

        elif (opcode == 8):
            inputArray[param3] = 1 if (inputArray[param1] == inputArray[param2]) else 0
            i += 4

        else:
            print("Error : unknown opcode at inputArray[" + repr(i) + "] (" +
                  repr(opcode) + ")", file=sys.stderr)
            sys.exit(1)

        end = end or (i >= len(inputArray))
    return


def partOne():
    print("Part one")
    process(1)


def partTwo():
    print("Part two")
    process(5)


partOne()
partTwo()

