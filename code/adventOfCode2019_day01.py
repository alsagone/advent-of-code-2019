import sys
import math

inputPath = "../input/input01.txt"


def readInput():
    inputArray = None

    try:
        inputArray = [int(line.rstrip('\n')) for line in open(inputPath, "r")]

    except:
        print("Error while reading " + inputPath, file=sys.stderr)
        sys.exit(1)

    return inputArray


"""
PART 1 
Fuel required to launch a given module is based on its mass.
Specifically, to find the fuel required for a module :
 - take its mass
 - divide by three
 - round down
 - and subtract 2
"""


def getFuel(mass):
    return (math.floor(mass/3.) - 2)


def partOne(inputArray):
    sum = 0.

    for mass in inputArray:
        sum += getFuel(mass)

    print("Answer part one : " + repr(sum))
    return


"""
PART 2 
So, for each module mass, calculate its fuel and add it to the total.
Then, treat the fuel amount you just calculated as the input mass and repeat the process
continuing until a fuel requirement is zero or negative.


    A module of mass 14 requires 2 fuel.
    This fuel requires no further fuel (2 divided by 3 and rounded down is 0, which would call for a negative fuel)
    so the total fuel required is still just 2.

    At first, a module of mass 1969 requires 654 fuel.
    Then, this fuel requires 216 more fuel (654 / 3 - 2). 216
    then requires 70 more fuel, which requires 21 fuel, which requires 5 fuel, which requires no further fuel.
    So, the total fuel required for a module of mass 1969 is 654 + 216 + 70 + 21 + 5 = 966.

    The fuel required by a module of mass 100756 and its fuel is: 33583 + 11192 + 3728 + 1240 + 411 + 135 + 43 + 12 + 2 = 50346.
"""


def recursiveFuelUtil(mass, sum):
    fuel = getFuel(mass)

    if (fuel <= 0):
        return sum

    sum += fuel
    return recursiveFuelUtil(fuel, sum)


def recursiveFuel(mass):
    return recursiveFuelUtil(mass, 0.)


def partTwo(inputArray):
    sum = 0.

    for mass in inputArray:
        sum += recursiveFuel(mass)

    print("Answer part two : " + repr(sum))


inputArray = readInput()
partOne(inputArray)
partTwo(inputArray)
